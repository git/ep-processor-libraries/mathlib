/*
 * cmpdriver.c
 *
 *  Created on: Jan 9, 2015
 *      Author: A Dean
 */

/* ======================================================================= */
/* cmpdriver.c - Common test driver utilities                                 */
/* ======================================================================= */

#include <math.h>
#include <stdlib.h>
#include <float.h>

#include "cmpdriver.h"

/* Global random data seed */
int seed;

/* Global profiling variables */
long long t_start;
long long t_stop;
long long t_offset;
long long cycle_counts[MTH_NUM_OUTPUT];

/* Global test results */
int fcn_pass[MTH_NUM_OUTPUT];
int all_pass;

/* Initialize testing */
void driver_init (char *str)
{
  /* Driver heading */
  printf ("\n\n\n\n");
  print_hline();
  printf ("Verification Results:  %s\n", str);
  print_hline();

  /* Initialize random data generation */
  seed = MTH_SEED_DEF;
 //if ((fp = fopen(MTH_SEED_FNAME, "r")) != NULL) {
 //   fscanf (fp, "%d", &seed);
 //   fclose(fp);
  //}
  srand (seed);
}


/* Display profiling results in a consistent manner */
void print_profile_results (char *str)
{
  printf ("Cycle Profile:  %s\n", str);
  print_hline ();
  printf ("C:       %d cycles\n", (int) cycle_counts[MTH_FCN_CI]);
  printf ("Inline:  %d cycles\n", (int) cycle_counts[MTH_FCN_INL]);
  printf ("Vector:  %d cycles\n", (int) cycle_counts[MTH_FCN_VEC]);
  print_hline ();
}

void print_profile_results2 (char *str)
{
  printf ("Cycle Profile:  %s\n", str);
  print_hline ();
  printf ("C:         %d cycles\n", (int) cycle_counts[MTH_FCN_CI]);
  printf ("Inline:    %d cycles\n", (int) cycle_counts[MTH_FCN_INL]);
  printf ("Vector:    %d cycles\n", (int) cycle_counts[MTH_FCN_VEC]);
  printf ("Inline V2: %d cycles\n", (int) cycle_counts[MTH_FCN_EXTRA]);
  print_hline ();
}


/* Display memory results in a consistent manner */
void print_memory_results (char *str)
{
	printf ("\n");
	print_hline ();
	printf ("Memory Profile:  %s\n", str);
	print_hline ();
	printf ("C:       %d bytes\n", (int) &kernel_ci_size);
	printf ("Vector:  %d bytes\n", (int) &kernel_vec_size);
	print_hline ();
}


/* Display test results in a consistent manner */
void print_test_results (int code)
{

}

void checkArraysDP(double x_real[], double x_img[], double y_real[],
		double y_img[], int entries)
{
	int i, pass;
	double x_value, y_value, diff_real, diff_img, rel_diff_real, rel_diff_img,
	zero, negZero;

	pass = 1;
	zero = 0;
	negZero = _lltod(0x8000000000000000);

	for (i = 0; i < entries; i++) {
		x_value = x_real[i];
		y_value = y_real[i];
		diff_real = y_value - x_value;
		rel_diff_real = diff_real / y_value;
		printf("%d. Real Part:\tAbsolute diff: %e,\tRelative diff: ", i+1,
				diff_real);
		if (y_value == zero || y_value == negZero) {
			printf("Not defined");
		} else {
			printf("%e", rel_diff_real);
			if (_fabs(rel_diff_real) > 1e-8) {
				pass = 0;
			}
		}

		x_value = x_img[i];
		y_value = y_img[i];
		diff_img = y_value - x_value;
		rel_diff_img = diff_img / y_value;
		printf("\n%d. Img Part:\tAbsolute diff: %e,\tRelative diff: ", i+1,
				diff_img);
		if (y_value == zero || y_value == negZero) {
			printf("Not defined\n");
		} else {
			printf("%e\n", rel_diff_img);
			if (_fabs(rel_diff_img) > 1e-8) {
				pass = 0;
			}
		}
	}

	print_hline();
	if (pass) {
		printf("Testing passed.\n");
	} else {
		printf("Testing failed to meet tolerance.\n");
	}
	print_hline();
}

void checkArraysSP(float x_real[], float x_img[], float y_real[], float y_img[],
		int entries)
{
	int i, pass;
	float x_value, y_value, diff_real, diff_img, rel_diff_real, rel_diff_img,
	zero, negZero;

	pass = 1;
	zero = 0;
	negZero = _itof(0x80000000);

	for (i = 0; i < entries; i++) {
		x_value = x_real[i];
		y_value = y_real[i];
		diff_real = y_value - x_value;
		rel_diff_real = diff_real / y_value;
		printf("%d. Real Part:\tAbsolute diff: %e,\tRelative diff: ", i+1, diff_real);
		if (y_value == zero || y_value == negZero) {
			printf("Not defined");
		} else {
			printf("%e", rel_diff_real);
			if (_fabsf(rel_diff_real) > 1e-4) {
				//printf("Failed %d\n", i);
				pass = 0;
			}
		}

		x_value = x_img[i];
		y_value = y_img[i];
		diff_img = y_value - x_value;
		rel_diff_img = diff_img / y_value;
		printf("\n%d. Img Part:\tAbsolute diff: %e,\tRelative diff: ", i+1, diff_img);
		if (y_value == zero || y_value == negZero) {
			printf("Not defined\n");
		} else {
			printf("%e\n", rel_diff_img);
			if (_fabsf(rel_diff_img) > 1e-4) {
				//printf("Failed %d\n", i);
				pass = 0;
			}
		}
	}

	print_hline();
	if (pass) {
		printf("Testing passed.\n");
	} else {
		printf("Testing failed to meet tolerance.\n");
	}
	print_hline();
}

void checkArraysWorkDP(double x[], double y[], int entries)
{
	int i, no_errors;
	double diff, x_value, y_value;
	printf("Begin checking results.\n");

	no_errors = 1;
	for (i = 0; i < entries; i+=2) {
		x_value = x[i];
		y_value = y[i];
		diff = x_value - y_value;
		if (diff > .01 || diff < -0.01) {
			if (i % 2 == 0) {
				printf("Error on the real portion of");
			} else {
				printf("Error on the imaginary portion of");
			}
			printf(" entry %d, expected %e, but got %e.\n", (i >> 1) + 1,
					y_value, x_value);
			no_errors = 0;
		}
	}

	if (no_errors) {
		printf("Testing successful.");
	}
}

void checkArraysWorkSP(float x[], float y[], int entries)
{
	int i, no_errors;
	float diff, x_value, y_value;
	printf("Begin checking results.\n");

	no_errors = 1;
	for (i = 0; i < entries; i+=2) {
		x_value = x[i];
		y_value = y[i];
		diff = x_value - y_value;
		if (diff > .01 || diff < -0.01) {
			if (i % 2 == 0) {
				printf("Error on the real portion of");
			} else {
				printf("Error on the imaginary portion of");
			}
			printf(" entry %d, expected %e, but got %e.\n", (i >> 1) + 1,
					y_value, x_value);
			no_errors = 0;
		}
	}

	if (no_errors) {
		printf("Testing successful.");
	}
}

void displaySpecialResultsDP(double x_real[], double x_img[], int entries)
{
	printf("Now showing special results.\n");

	int i;

	for (i = 0; i < entries; i++) {
		printf("Entry: %d, Real part: %e, Imaginary part: %e\n",
				i+1, x_real[i], x_img[i]);
	}
}

void displaySpecialResultsSP(float x_real[], float x_img[], int entries)
{
	printf("Now showing special results.\n");

	int i;

	for (i = 0; i < entries; i++) {
		printf("Entry: %d, Real part: %e, Imaginary part: %e\n",
				i+1, x_real[i], x_img[i]);
	}
}

void printArrayDP(double x[], int entries, char *str) {
	int i, line;
	line = 1;

	printf("\n%s = [\n", str);

	for (i = 0; i < entries; i++, line++) {
		printf("%.*e, ", DBL_DIG, x[i]);
		if (line == 5) {
			printf("...\n");
			line = 0;
		}
	}

	printf("];\n");
}

void printArraySP(float x[], int entries, char *str) {
	int i, line;
	line = 1;

	printf("\n%s = [\n", str);

	for (i = 0; i < entries; i++, line++) {
		printf("%.*e, ", FLT_DIG, x[i]);
		if (line == 5) {
			printf("...\n");
			line = 0;
		}
	}

	printf("];\n");
}

double gimme_random_DP (double range, double offset)
{
  return ((double) rand() / (double) RAND_MAX * range + offset);
}

float gimme_random_SP (float range, float offset)
{
	return ((float) rand() / (float) RAND_MAX * range + offset);
}

void printArrayExcelDP(double x[], double y[], int entries, int type)
{
	int i;

	if (type) {
		for (i = 0; i < entries; i++) {
			printf("%d\t%f\t%f\n", i+1, x[i], y[i]);
		}
	} else {
		for (i = 0; i < entries; i++) {
			printf("%e\n", x[i]);
		}
	}
}

void printArrayExcelSP(float x[], float y[], int entries, int type)
{
	int i;

	if (type) {
		for (i = 0; i < entries; i++) {
			printf("%d\t%f\t%f\n", i+1, x[i], y[i]);
		}
	} else {
		for (i = 0; i < entries; i++) {
			printf("%e\n", x[i]);
		}
	}
}

void fileResultsDP(double x_real[], double x_img[], FILE *f, int entries)
{
	int i;
	for (i = 0; i < entries; i++) {
		fprintf(f, "%.*e + i*%.*e\n", DBL_DIG, x_real[i], DBL_DIG, x_img[i]);
	}
	fprintf(f, "\n");
}

void fileResultsSP(float x_real[], float x_img[], FILE *f, int entries)
{
	int i;
	for (i = 0; i < entries; i++) {
		fprintf(f, "%.*e + i*%.*e\n", FLT_DIG, x_real[i], FLT_DIG, x_img[i]);
	}
	fprintf(f, "\n");
}

/* ======================================================================== */
/*  End of file: cmpdriver.c                                                   */
/* ======================================================================== */


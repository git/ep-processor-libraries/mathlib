/* ======================================================================== *
 * MATHLIB -- TI Floating-Point Math Function Library                       *
 *                                                                          *
 *                                                                          *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/   *
 *                                                                          *
 *                                                                          *
 *  Redistribution and use in source and binary forms, with or without      *
 *  modification, are permitted provided that the following conditions      *
 *  are met:                                                                *
 *                                                                          *
 *    Redistributions of source code must retain the above copyright        *
 *    notice, this list of conditions and the following disclaimer.         *
 *                                                                          *
 *    Redistributions in binary form must reproduce the above copyright     *
 *    notice, this list of conditions and the following disclaimer in the   *
 *    documentation and/or other materials provided with the                *
 *    distribution.                                                         *
 *                                                                          *
 *    Neither the name of Texas Instruments Incorporated nor the names of   *
 *    its contributors may be used to endorse or promote products derived   *
 *    from this software without specific prior written permission.         *
 *                                                                          *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   *
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    *
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   *
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        *
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   *
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY   *
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT     *
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    *
 * ======================================================================== */
  
/**
 *  @mainpage 
 *  <p align="center"> 
 *  <a href="#Introduction">Introduction</a>,
 *  <a href="#Documentation">Documentation</a>,
 *  <a href="#Whats_New">What's New</a>
 *  <a href="#Compatibility">Upgrade and Compatibility Information</a>,
 *  <a href="#Host_Support">Host Support</a>,
 *  <a href="#Device_Support">Device Support</a>,
 *  <a href="#Validation">Validation Information</a>,
 *  <a href="#Known_Issues">Known Issues</a>,
 *  <a href="#Version">Version Information</a>,
 *  <a href="#Support">Technical Support</a>
 *  </p>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Introduction***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *
 *  <hr> 
 *  <h2><a name="General_Info">Introduction</a></h2>
 *  The Texas Instruments MATHLIB is an optimized floating-point math function library for C programmers 
 *  using TI floating point devices.
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Documentation**************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *  <hr>
 *
 *  <h2><a name="Documentation">Documentation</a></h2>
 *  <p>The following documentation is available: </p> 
 *  - <a href="docs/doxygen/MATHLIB_Function_Reference.chm">MATHLIB Function Reference</a>
 *
 *  <p>Release notes from previous releases are also available in the
 *  <a href="docs/relnotes_archive">relnotes_archive</a>
 *  directory.</p>
 *
 *  <a href="#XDC_TOP">back to top</a> 
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*************************************What's New***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Whats_New">What's New</a></h2>
 *  <p> The following requirements have been met:
 *  - CATREQ-602: MISRA-C 2004 compliance for MATHLIB.
 *  </p>
 *  <p> The following bugs have been resolved:
 *  - SDOCM00121788: c66x 'divsp' kernel incorrect prototype types
 *  </p>
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--************************Upgrade and Compatibility Information*************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Compatibility">Upgrade and
 *  Compatibility Information</a></h2>
 *
 *  This <b>ti.mathlib</b> is the initial RTSC packaged version.  
 *
 *
 *  Please note that the package compatibility keys are independent of XDC product release numbers. 
 * 
 *  Package compatibility keys are intended to: 
 * 
 *   -# Enable tooling to identify incompatibilities between components, and 
 *   -# Convey a level of compatibility between different releases to set end user expectations. 
 * 
 * Package compatibility keys are composed of 4 comma-delimited numbers - M, S, R, P - where: 
 *
 * - <b>M = Major</b> - A difference in M indicates a break in compatibility. The package consumer is required to re-write its source code in order to use the package.
 * - <b>S = Source</b> - A difference in S indicates source compatibility. The package consumerís source code doesn't require change, but does require a recompile. 
 * - <b>R = Radix</b> - A difference in R indicates an introduction of new features, but compatibility with previous interfaces is not broken. If libraries are provided by the package, an application must re-link with the new libraries, but is not required to recompile its source.
 * - <b>P = Patch</b> - A difference in P indicates that only bugs have been fixed in the latest package and no new features have been introduced. If libraries are provided by the package, an application must re-link with the new libraries, but is not required to recompile its source. 
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--****************************************Host Support**********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *  <hr>
 *
 *  <h2><a name="Host_Support">Host Support</a></h2>
 *
 *  This release supports the following hosts:
 *
 *   - Windows XP & Windows 7
 *   - Linux
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*****************************************Device Support*******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Device_Support">Device Support</a></h2>
 *
 *  This release supports the following device families: 
 *  - C66x 
 *  - C674x 
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**********************************Validation Information******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *
 *  <hr>
 *
 *  <h2><a name="Validation">Validation Information</a></h2>
 *
 *  This release was built and validated using the following tools:
 *
 *  <ul>
 *    <li> XDC Tools version 3.25.00.48
 *    <li> C6x Code Generation Tools version 7.4.2
 *    <li> CCS 5.5.0
 *  </ul>
 *
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Known Issues***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *  <hr>
 *
 *  <h2><a name="Known_Issues">Known Issues</a></h2>
 *  <ul>
 *    <li> C66 assembly implementation of Mathlib kernels is not available in this release. The assembly 
 *         version of each kernel is exactly the same as the C version. Please ignore 
 *         the profiling results for assembly version.
 *    <li> Kernels acossp, asinsp, atanhdp and atanhsp are not included in the Mathlib RTS build,
 *         since C6000 RTS has faster implementation than Mathlib for these kernels.
 *  </ul>
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <hr>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************** Versioning **********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *  <h2><a name="Version">Version Information</a></h2>
 *
 *  This product's version follows a version format, <b>M.m.p.b</b>,
 *  where <b>M</b> is a single digit Major number, <b>m</b> is single digit minor number,
 *  <b>p</b> is a single digit patch number and <b>b</b> is an unrestricted set of digits used as an incrementing build counter. 
 *
 *  <p>Please note that version numbers and compatibility keys are
 *  NOT the same. For an explanation of compatibility keys, please refer to
 *  the '<a href="#Compatibility">Upgrade and Compatibility Information</a>' section.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*********************************** Technical Support ********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *  <h2><a name="Support">Technical Support</a></h2>
 *
 *  Questions regarding the MATHLIB library should be directed to corresponding
 *  device forums:
 *  - C66x:  <a href="http://e2e.ti.com/support/dsp/c6000_multi-core_dsps/f/639">
 *           Keystone Multicore Forum (C66, etc)</a>. Please include the text 
 *           "MATHLIB" in the title and add "C66x" and "MATHLIB" tags to your post.
 *  - C674x: <a href="http://e2e.ti.com/support/dsp/tms320c6000_high_performance_dsps/f/115">
 *           C67x Single Core DSP Forum </a>. Please include the text "MATHLIB" 
 *           in the title and add "C674x" and "MATHLIB" tags to your post.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *
 */

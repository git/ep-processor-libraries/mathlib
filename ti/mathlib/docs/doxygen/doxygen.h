/* ======================================================================== *
 * MATHLIB -- TI Floating-Point Math Function Library                       *
 *                                                                          *
 *   Doxygen generation header file                                         *
 *                                                                          *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/   *
 *                                                                          *
 *                                                                          *
 *  Redistribution and use in source and binary forms, with or without      *
 *  modification, are permitted provided that the following conditions      *
 *  are met:                                                                *
 *                                                                          *
 *    Redistributions of source code must retain the above copyright        *
 *    notice, this list of conditions and the following disclaimer.         *
 *                                                                          *
 *    Redistributions in binary form must reproduce the above copyright     *
 *    notice, this list of conditions and the following disclaimer in the   *
 *    documentation and/or other materials provided with the                *
 *    distribution.                                                         *
 *                                                                          *
 *    Neither the name of Texas Instruments Incorporated nor the names of   *
 *    its contributors may be used to endorse or promote products derived   *
 *    from this software without specific prior written permission.         *
 *                                                                          *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   *
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    *
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   *
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        *
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   *
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY   *
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT     *
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    *
 * ======================================================================== */

/**
 * @mainpage  MATHLIB Function Reference
 *
 *
 * @HLINE
 *
 *
 * @section content  Contents
 *  
 *  <ol>
 *    <li> @ref intro
 *    <li> @ref modules
 *    <li> @ref benefits
 *    <li> @ref funcref
 *      <ul>
 *        <li> @subpage double_precision
 *        <li> @subpage single_precision
 *      </ul>
 *  </ol>
 *  
 *  
 * @HLINE
 *
 *
 * @section intro  Introduction
 *
 *  The Texas Instruments math library is an optimized floating-point math 
 *  function library for C programmers using TI floating point devices.  These 
 *  routines are typically used in computationally intensive real-time applications 
 *  where optimal execution speed is critical. By using these routines instead of 
 *  the routines found in the existing run-time-support libraries, you can achieve 
 *  execution speeds considerably faster without rewriting existing code. The
 *  MATHLIB library includes all floating-point math routines currently provided 
 *  in existing run-time-support libraries. These new functions can be called with 
 *  the current run-time-support library names or the new names included in the 
 *  math library.
 *  
 * @HLINE
 *
 *
 * @section modules Modules
 *
 *  The rich set of software routines included in MATHLIB are organized into 
 *  two categories:
 *  
 *    -# Single precision floating point
 *    -# Double precision floating point
 *  
 * @HLINE
 *
 *
 * @section benefits Features and Benefits
 *  - Natural C Source Code
 *  - Optimized C code with Intrinsics
 *  - Hand-coded assembly-optimized routines
 *  - C-callable routines, which can be inlined and are fully compatible with the TMS320C6000 compiler
 *  - Routines which accept single sample or vector inputs
 *  - Provided functions are tested against C model and existing run-time-support functions
 *  - Benchmarks (cycle and code size)
 *  - The provided precompiled library was compiled using Code Generation Tools v7.4.2
 *  
 * @HLINE
 *
 *
 * @section funcref  Functional Reference
 *
 *  The following sections provide a detailed reference of all functions within 
 *  MATHLIB.  The functions are organized into the following two categories:
 *
 *  - @subpage double_precision
 *  - @subpage single_precision
 * 
 */

/**
 * @page double_precision Double Precision 
 *
 *  This section provides a description of the functions that are double precision.
 * 
 *  @section dpf Double Precision Functions
 *
 * The routines included in the Math library are provided as both single- and double-precision versions. 
 * DP is used to identify the double-precision functions.
 *
 *  - @ref acosdp
 *  - @ref acoshdp
 *  - @ref asindp
 *  - @ref asinhdp
 *  - @ref atan2dp
 *  - @ref atandp
 *  - @ref cosdp
 *  - @ref coshdp
 *  - @ref divdp
 *  - @ref exp10dp
 *  - @ref exp2dp
 *  - @ref expdp
 *  - @ref log10dp
 *  - @ref log2dp
 *  - @ref logdp
 *  - @ref powdp
 *  - @ref recipdp
 *  - @ref rsqrtdp
 *  - @ref sindp
 *  - @ref sinhdp
 *  - @ref sqrtdp 
 *  - @ref tandp
 *  - @ref tanhdp
 *
 */

/**
 * @page single_precision Single Precision 
 *
 *  This section provides a description of the functions that are single precision.
 * 
 *  @section spf Single Precision Functions
 *
 * The routines included in the Math library are provided as both single- and double-precision versions. 
 * SP is used to identify the single-precision functions.
 *
 *  - @ref acoshsp
 *  - @ref acossp
 *  - @ref asinhsp
 *  - @ref asinsp
 *  - @ref atan2sp
 *  - @ref atanhsp
 *  - @ref atansp
 *  - @ref coshsp
 *  - @ref cossp
 *  - @ref divsp
 *  - @ref exp10sp
 *  - @ref exp2sp
 *  - @ref expsp
 *  - @ref log10sp
 *  - @ref log2sp
 *  - @ref logsp
 *  - @ref powsp
 *  - @ref recipsp
 *  - @ref rsqrtsp
 *  - @ref sinhsp
 *  - @ref sinsp
 *  - @ref sqrtsp 
 *  - @ref tanhdp
 *  - @ref tandp
 *
 */


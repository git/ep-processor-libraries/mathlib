# ==============================================================================
# File            : MakedocsBundle.mk
# Description     : 
#
#   GNU makefile to generate MATHLIB bundle documentation using Doxygen
#
# ==============================================================================


#SWTOOLS_PATH must be defined by setup scripts

DOXPATH = docs/doxygen
DOXHTML = $(DOXPATH)/html
ECOHTML = $(DOXPATH)/html
SWMHTML = $(SWMPATH)/html

TI_DOXYGEN_TEMPLATES ?= ../docs/doxygen/TI_Templates

# Document targets 
USAGE_DOC          = ./docs/MATHLIB_Users_Manual.chm
RELEASE_DOC_BUNDLE = ./$(DOXPATH)/release.chm
#ECLIPSE_DIR        = ./eclipse/plugins

# Redirection script
REDIRECT = $(SWTOOLS_PATH)/redirect.js

release: $(USAGE_DOC) #$(ECLIPSE_DIR) #blw! $(MANIFEST_DOC) USE SRAS-GENERATED MANIFEST

releasenotes: $(RELEASE_DOC_BUNDLE)


#$(ECLIPSE_DIR): ./eclipse/mathlib.xml ./eclipse/mathlib_toc.xml
#	-@echo generating Eclipse Plugin ...
#	-xs xdc.tools.eclipsePluginGen -o . -x ./eclipse/mathlib.xml -c ./eclipse/mathlib_toc.xml

$(RELEASE_DOC_BUNDLE): ./docs/doxygen/release.h
	-@echo generating Release Notes ...
	if test ! -d ./docs/doxygen/tmp; then mkdir ./docs/doxygen/tmp; fi
	cp $(TI_DOXYGEN_TEMPLATES)/*.* ./docs/doxygen/tmp
	doxygen $(SWTOOLS_PATH)/docs/doxygen/bundlerelDoxyfile
	@xs -f $(SWTOOLS_PATH)/bundlerelease.js
	$(RMDIR) ./docs/doxygen/tmp

genbundledocs $(USAGE_DOC): ./$(DOXPATH)/doxygen.h
	-@echo Generating MATHLIB bundle documentation ...
	-@echo copying tirex folder
	cp -r ../.metadata .
	if test ! -d $(DOXHTML); then mkdir $(DOXHTML); fi
	cp $(TI_DOXYGEN_TEMPLATES)/*.* $(DOXHTML)
	@echo Pulling in MATHLIB API Documentation ...
	@cp -fru ../../mathlib/$(ECOHTML) $(DOXHTML)/mathlib_html
	doxygen ./$(DOXPATH)/Doxyfile
ifeq ($(IS_RTS), 1)
	xs -f $(REDIRECT) ./doxygen/html/index.html > ./docs/MATHLIB_RTS_Users_Manual.html
else
	xs -f $(REDIRECT) ./doxygen/html/index.html > ./docs/MATHLIB_Users_Manual.html
endif


# End of Makedocs.mk

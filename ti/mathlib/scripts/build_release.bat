@echo on
:: *************************************************************************
::  FILE           : build_release.bat
::  DESCRIPTION    :
::
::     This batch script creates the full MATHLIB release
::
:: *************************************************************************

@echo Executing:  %~fn0

:: *************************************************************************
:: *** Initialize build environment variables
:: *************************************************************************
set MATHLIB_HOME_DIR=%~dp0\..
set MATHLIB_BUILD_DIR=%MATHLIB_HOME_DIR%\artifacts
set MATHLIB_OUTPUT_DIR=%MATHLIB_BUILD_DIR%\output
set MATHLIB_LOG_DIR=%MATHLIB_BUILD_DIR%\logs
set MATHLIB_TEST_DIR=%MATHLIB_BUILD_DIR%\test
set MATHLIB_FILE_LIST=%MATHLIB_OUTPUT_DIR%\build_targets
set XDCOPTIONS=

set MATHLIB_BUILD_C66=0
set MATHLIB_BUILD_C674=0
set MATHLIB_C674_TEST_RPT=MATHLIB_c674x_TestReport.html
set MATHLIB_C674_SRC_LOG=%MATHLIB_LOG_DIR%\build_C674_src.log
set MATHLIB_C674_RTS_SRC_LOG=%MATHLIB_LOG_DIR%\build_C674_rts_src.log
set MATHLIB_C66_TEST_RPT=MATHLIB_c66x_TestReport.html
set MATHLIB_C66_RTS_SRC_LOG=%MATHLIB_LOG_DIR%\build_C66_rts_src.log
set MATHLIB_C66_SRC_LOG=%MATHLIB_LOG_DIR%\build_C66_src.log
set MATHLIB_C66_MISRA_DOC=MATHLIB_c66x_MisraC_Policy.xlsx

:: *************************************************************************
:: *** Clear artifacts list if it exists
:: *************************************************************************
if exist "%MATHLIB_FILE_LIST%" (
  del /F "%MATHLIB_FILE_LIST%"
)


:: *************************************************************************
:: *** Check command line input
:: *************************************************************************
if "%1" == ""      goto build_all
if "%1" == "all"   goto build_all
if "%1" == "C674"  goto build_C674
if "%1" == "C66"   goto build_C66

:: USAGE:
echo ERROR: Option "%1" supplied is invalid...
echo .
echo . Usage: %0 [all C674 C66] (defaults to "all")
echo .
echo .     "all"  : Build all targets 
echo .     "C674" : Build just the C674 MATHLIB targets 
echo .     "C66"  : Build just the C66x MATHLIB targets 
goto end


:: *************************************************************************
:: *** Set environment variables based on "all"
:: *************************************************************************
:build_all
set MATHLIB_BUILD_C66=1
set MATHLIB_BUILD_C674=1
goto build_process

:: *************************************************************************
:: *** Set environment variables based on a specific target
:: *************************************************************************
:build_C674
set MATHLIB_BUILD_C674=1
goto build_process



:: *************************************************************************
:: *** Set environment variables based on a specific target
:: *************************************************************************
:build_C66
set MATHLIB_BUILD_C66=1
goto build_process


:build_process

:: *************************************************************************
:: *** Create temporary directory for installers 
:: *************************************************************************
if exist "%MATHLIB_BUILD_DIR%" (
  rmdir /S /Q "%MATHLIB_BUILD_DIR%"
)
mkdir "%MATHLIB_BUILD_DIR%"
mkdir "%MATHLIB_OUTPUT_DIR%"
mkdir "%MATHLIB_LOG_DIR%"
mkdir "%MATHLIB_TEST_DIR%"


:build_process_C674
:: *************************************************************************
:: *** C64Px build process
:: *************************************************************************
if "%MATHLIB_BUILD_C674%" == "0" goto build_process_C66

:: *************************************************************************
:: *** Build C674x MATHLIB Source delivery
:: *************************************************************************
cd "%MATHLIB_HOME_DIR%"
xdc clean > "%MATHLIB_C674_SRC_LOG%"
xdc XDCARGS="c674x src bundle install" >> "%MATHLIB_C674_SRC_LOG%" 2>&1

:: Verify if artifacts created successfully
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %MATHLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %MATHLIB_FILE_LIST%
	    )
)
copy "mathlib_c674x"*".bin" "%MATHLIB_OUTPUT_DIR%"
copy "mathlib_c674x"*".exe" "%MATHLIB_OUTPUT_DIR%"

:: install MATHLIB C674x version which is need to build RTS projects
mathlib_c674x_3_1_2_1_Win32.exe --mode silent

xdc clean > "%MATHLIB_C674_RTS_SRC_LOG%"
set IS_RTS=1
xdc XDCARGS="c674x override_rts src bundle install" >> "%MATHLIB_C674_RTS_SRC_LOG%" 2>&1
set IS_RTS=0

:: Verify if artifacts created successfully
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %MATHLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %MATHLIB_FILE_LIST%
	    )
)
copy "mathlib_rts_c674x"*".bin" "%MATHLIB_OUTPUT_DIR%"
copy "mathlib_rts_c674x"*".exe" "%MATHLIB_OUTPUT_DIR%"

echo %MATHLIB_C674_TEST_RPT% >> %MATHLIB_FILE_LIST%
copy "docs\bundle\%MATHLIB_C674_TEST_RPT%" "%MATHLIB_OUTPUT_DIR%"

  

:build_process_C66
:: *************************************************************************
:: *** C66x build process
:: *************************************************************************
if "%MATHLIB_BUILD_C66%" == "0" goto webgen

:: *************************************************************************
:: *** Build C66x MATHLIB Source delivery
:: *************************************************************************
cd "%MATHLIB_HOME_DIR%"
xdc clean > "%MATHLIB_C66_SRC_LOG%"
xdc XDCARGS="c66x src bundle install" >> "%MATHLIB_C66_SRC_LOG%" 2>&1

:: Verify if artifacts created successfully
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %MATHLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %MATHLIB_FILE_LIST%
	    )
)
copy "mathlib_c66x"*".bin" "%MATHLIB_OUTPUT_DIR%"
copy "mathlib_c66x"*".exe" "%MATHLIB_OUTPUT_DIR%"

:: install MATHLIB C66x version which is need to build RTS projects
mathlib_c66x_3_1_2_1_Win32.exe --mode silent

xdc clean > "%MATHLIB_C66_RTS_SRC_LOG%"
set IS_RTS=1
xdc XDCARGS="c66x override_rts src bundle install" >> "%MATHLIB_C66_RTS_SRC_LOG%" 2>&1
set IS_RTS=0

:: Verify if artifacts created successfully
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %MATHLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %MATHLIB_FILE_LIST%
	    )
)
copy "mathlib_rts_c66x"*".bin" "%MATHLIB_OUTPUT_DIR%"
copy "mathlib_rts_c66x"*".exe" "%MATHLIB_OUTPUT_DIR%"

echo %MATHLIB_C66_TEST_RPT% >> %MATHLIB_FILE_LIST%
copy "docs\bundle\%MATHLIB_C66_TEST_RPT%" "%MATHLIB_OUTPUT_DIR%"
echo %MATHLIB_C66_MISRA_DOC% >> %MATHLIB_FILE_LIST%
copy "docs\bundle\%MATHLIB_C66_MISRA_DOC%" "%MATHLIB_OUTPUT_DIR%"


:: ************************************************************************
:: ************************** Webgen **************************************
:: ************************************************************************
:webgen
:: Copy Webgen folder 
if exist "%MATHLIB_BUILD_DIR%\webgen" (
  del /F "%MATHLIB_BUILD_DIR%\webgen"
)
cp -r "%MATHLIB_HOME_DIR%\webgen" "%MATHLIB_BUILD_DIR%\webgen"

:: Copy Manifest
::blw! cp -f docs\manifest\Software_Manifest.html  "%MATHLIB_OUTPUT_DIR%\Software_Manifest.html"


:: Copy Function reference
cp -f docs\doxygen\MATHLIB_Function_Reference.chm  "%MATHLIB_OUTPUT_DIR%\MATHLIB_Function_Reference.chm"

:: *************************************************************************
:: *** Cleanup and return
:: *************************************************************************
:end

cd "%MATHLIB_HOME_DIR%"
set MATHLIB_HOME_DIR=
set MATHLIB_BUILD_DIR=
set MATHLIB_OUTPUT_DIR=
set MATHLIB_LOG_DIR=
set MATHLIB_TEST_DIR=
set MATHLIB_C66_TEST_RPT=
set MATHLIB_C674_TEST_RPT=
set MATHLIB_BUILD_C66=
set MATHLIB_BUILD_C674=
set MATHLIB_C66_SRC_LOG=
set MATHLIB_C674_SRC_LOG=
set MATHLIB_C66_MISRA_DOC=
set XDCOPTIONS=

:: *************************************************************************
:: *** Nothing past this point
:: *************************************************************************


####----------------------------------------------------------------------------
### IMPORTS
import sys
import os
import re
import itertools
import subprocess
import json
##------------------------------------------------------------------------------

####----------------------------------------------------------------------------
### CONFIGURATION

# Build Information
build_conf  = "Release"
library     = "mathlib"
version     = "3_1_2_1"
out_base    = "./out"
log_gen     = "test_report_log.txt"
json_gen    = "TEST_REPORT.json"
num_cores    = 1
cg_tools_version = '7.4.2'

## Project Build Types
processors     = {
    '66':'c66',
    '674':'c674'
}
endiannesses   = {
    'BE':'BE_Sim.ccxml',
    'LE':'LE_Sim.ccxml'
}
object_formats = [
    'ELF',
    'COFF'
]
precisions  = {
    'dp':"Double Float",
    'sp':"Single Float"
}

## Combinations
project_types  = list(itertools.product(*[endiannesses.keys(), object_formats]))
identify_types = list(itertools.product(*[precisions.keys(), endiannesses.keys(), object_formats]))

## Path Bases
install_base     = 'c:/ti'
tools_base       = 'c:/MATHLIB_Tools'
target_conf_base = './res'

## General Locations
src_gen       = '/'.join(['packages/ti', library, 'src'])
ccsv5_gen     = 'CCSv5_5_0/ccsv5'
loadti_gen    = 'ccs_base/scripting/examples/loadti/loadti.bat'

ccsv5_base = '/'.join([tools_base, ccsv5_gen])
eclipse_base  = '/'.join([ccsv5_base, 'eclipse'])

## Executable Paths
loadti_exe   = '/'.join([ccsv5_base, loadti_gen])
eclipsec_exe = '/'.join([eclipse_base, 'eclipsec'])

## File Paths
ccs_config_file = '/'.join([eclipse_base, 'ccs_config.xml'])

##------------------------------------------------------------------------------


####----------------------------------------------------------------------------
### MAIN

def main(argv):

    # should be more generalized...but i doubt this will be used again
    if argv[0] == "all":
        procs = [proc for proc in processors.keys()]
    elif argv[0] == "c66":
        procs = ['66']
    elif argv[0] == "c674":
        procs = ['674']
        
    ### Setup main data structure
    bench_data = init_data_structure_from_config()

    for proc in procs:
        ## Build filepaths
        jname = '_'.join([processors[proc], json_gen])
        jsonfile = '/'.join([out_base, jname])
        lname = '_'.join([processors[proc], log_gen])
        logfile = '/'.join([out_base, lname])
        
        ## Clear log file
        if os.path.isfile(logfile): os.remove(logfile)

        srcdir = get_library_src_path(proc)
        print srcdir
        
        for kernel in os.listdir(srcdir):
            ## Grab precision string
            prec_re = re.compile(r"[a-zA-Z0-9]*(?P<precision>dp|sp)")
            mat = re.match(prec_re, kernel)
            
            # check kernel name valid
            if not mat or mat.group('precision') not in precisions:
                continue
            
            precision = precisions[mat.group('precision')]
            
            ## Build and simulate all configuration combinations
            for (endianness, object_format) in project_types:

                # hardcoding because it is exceptional
                if proc == '674' and endianness == 'BE':
                    continue

                
                # Throw together major identificaion string
                PEO = precision[0] + endianness[0] + object_format[0]
                PEO = PEO.upper()
                
                target_conf_path = '/'.join([target_conf_base, endiannesses[endianness]])
                
                bench_data[PEO]['test_name'] = PEO
                bench_data[PEO]['Precision']  = precision
                bench_data[PEO]['Endianness'] = endianness
                bench_data[PEO]['Object File Format'] = object_format
                bench_data[PEO]['Simulator']  = get_xml_attribute(target_conf_path,
                                                                  'instance', 'id')
                
                simout = build_and_simulate_kernel(kernel, proc, endianness,
                                                   object_format, logfile)
                bench_data[PEO]['Kernels'][kernel] = parse_simulation(simout)
                
        with open(jsonfile, 'w') as jf:
            jf.write(json.dumps(bench_data))
            
##------------------------------------------------------------------------------


####----------------------------------------------------------------------------
### FUNCTION DEFINITIONS

def get_xml_attribute(xmlfile, tag_re, attr_re, style_re = '[^"]*'):
    """Returns first instance of specified attribute string"""

    full_re = '<' + tag_re.strip() + " .* " + attr_re.strip() + '="(?P<attr>' + style_re + ')"'
    full_re = re.compile(full_re)
    
    with open(xmlfile, 'r') as f:
        for line in f:
            mat = re.match(full_re, line)
            if mat:
                return mat.group('attr')
                
def get_cg_tools_version(xmlfile):
    return "7.4.2" # CHANGE THIS

def get_num_cores():
    return num_cores

def init_data_structure_from_config():
    """Creates data structure and populates with info from config"""    
    ## Initialize major identificaion tier of data dict as minor dict

    data = {}
    
    for (precision, endianness, object_format) in identify_types:
        PEO = precision[0] + endianness[0] + object_format[0]
        PEO = PEO.upper()
        data[PEO] = {}
        data[PEO]['Cores Used'] = get_num_cores()
        data[PEO]['CCS Version'] = get_xml_attribute(ccs_config_file,
                                                           'Edition',
                                                           'version',
                                                           '[.0-9]*')
        data[PEO]['CG TOOLS Version'] = get_cg_tools_version(None)
        data[PEO]['Kernels'] = {}

    return data

def get_library_src_path(processor):
    lib_full_name = '_'.join([library, processors[processor] + 'x', version])
    return '/'.join([install_base, lib_full_name, src_gen])
    


def build_and_simulate_kernel(kernel, processor, endianness, object_format,logfile):
    """Builds a kernel in CCS and then simulates using proper target configuration

    Args:
    - kernel --------- kernel which should be tested
    - processor ------ processor core to simulate
    - endianness ----- processor endianness
    - object_format -- compiled object code format

    Return: Simulation output as string
    """

    if processor == '674':
        processor_name = processor + endianness

    project_name = '_'.join([kernel, processor_name, endianness, object_format])
    
    ### build directory tree strings    
    lib_src_path = get_library_src_path(processor)
    
    kernel_workspace_path  = '/'.join([lib_src_path, kernel, processors[processor]])
    kernel_project_path    = '/'.join([kernel_workspace_path, project_name])
    kernel_executable_path = '/'.join([kernel_project_path, build_conf, project_name + ".out"])

    target_conf_path = '/'.join([target_conf_base, endiannesses[endianness]])

    ### Build and simulate, write STDOUT to log, STDERR to screen
    with open(logfile,'a') as log:
        print "building " + project_name + "..."
        log.write("building " + project_name + "...")
        subprocess.call([
            eclipsec_exe,
            "-noSplash",
            "-data", kernel_workspace_path,
            "-application", "com.ti.ccstudio.apps.projectBuild",
            "-ccs.projects", project_name,
            "-ccs.configuration", "Release",
            "-ccs.autoImport"
        ], stdout=log, stderr=log)

        print "simulating " + project_name + "..."
        log.write("simulating " + project_name + "...")
        output = subprocess.check_output([
            loadti_exe,
            "-c", target_conf_path,
            "-n", kernel_executable_path
        ])

        log.write(output)

    return output.splitlines()

def parse_simulation(simout):
    """Grabs benchmark and build information from the simulation and adds it to the data dict"""

    local_data = {}
    
    default_result = 'RESULT ERR'

    ## Fill with initial data
    local_data['Result'] = default_result
    local_data['Profile'] = {}
    local_data['Memory'] = {}

    ## Precomile regular expressions
    # Pass/Fail information with specific errors on failure
    result_re = re.compile(  r"[VPSERC][a-z].* Data.*:\s+" +
                             r"(?P<result>Failed \(.*\)|Passed)")
    
    # Cycle/Memory usage information
    bench_re = re.compile(     r"(?P<test>RTS|ASM|C|Inline|Vector):" +
                               r"\s+(?P<num>[0-9]*) (?P<unit>cycles|bytes)")
    
    for line in simout:
        result_mat  = re.match(result_re, line)
        bench_mat   = re.match(bench_re, line)

        if result_mat:
            # Always update if fails with longest error message found
            if (result_mat.group('result') and
                (local_data['Result'] == default_result or
                 len(result_mat.group('result')) > len(local_data['Result']))):
                local_data['Result'] = result_mat.group('result')


        if bench_mat:
            if bench_mat.group('unit') == 'cycles':
                local_data['Profile'][bench_mat.group('test')] = bench_mat.group('num')
            else:
                local_data['Memory'][bench_mat.group('test')] = bench_mat.group('num')

    return local_data
    
##------------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])

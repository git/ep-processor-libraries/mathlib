import sys
import json

####----------------------------------------------------------------------------
### CONFIGURATION

library = 'MATHLIB'
version = '3.1.2.1'

processors     = {
    '66':'c66',
    '674':'c674'
}

## File Paths
out_base = "./out"
html_gen = "TestReport.html"
json_gen = "TEST_REPORT.json"

## Color Constants
red_rgb   = '#cc6666'
green_rgb = '#66cc66'

## Fields of Interest
profile_fields = [
    'RTS',
    #'ASM',
    'C',
    'Inline',
    'Vector'
]
memory_fields = [
    #'ASM',
    'C',
    'Vector'
]

##------------------------------------------------------------------------------

####----------------------------------------------------------------------------
### NOTES

NOTES = [
    #"Assembly implementation is not available in this release. The assembly version of each kernel is exactly the same as the C version. Please ignore the profiling results for assembly version.",
    
    "Vector cycles are calculated for 128-iteration loop."
]

##------------------------------------------------------------------------------


####----------------------------------------------------------------------------
### MAIN

def main(argv):

    # should be more generalized...but i doubt this will be used again
    if argv[0] == "all":
        procs = [proc for proc in processors.keys()]
    elif argv[0] == "c66":
        procs = ['66']
    elif argv[0] == "c674":
        procs = ['674']
        
    for proc in procs:
        ## Build filepaths
        jname = '_'.join([processors[proc], json_gen])
        jsonfile = '/'.join([out_base, jname])
        hname = '_'.join([library, processors[proc] + 'x', html_gen])
        htmlfile = '/'.join([out_base, hname])
    
        with open(jsonfile, 'r') as jf:
            bench_data = json.loads(jf.read())
            
        if bench_data:
            with open(htmlfile, 'w') as hf:
                print "Generating report...",
                for line in generate_report(bench_data, processors[proc] + 'x'):
                    hf.write(line)
                print "DONE"
                print "See output in", htmlfile
        else:
            print "ERROR"
        
##------------------------------------------------------------------------------    
# width of the kernel name + results column on the left side
def info_width():
    return 2

# number of benchmark tests (i.e. named test columns)
def bench_width():
    return len(profile_fields) + len(memory_fields)

# full width of the table
def full_width():
    return bench_width() + info_width()

def format_table_header_entry(key, value):
    """Formats table header entires"""

    outarr = []
    outarr.append('<tr align="left" bgcolor="#ffffcc">')
    outarr.append('<th colspan="' + str(info_width()) + '">' + str(key) + ':</th>')
    outarr.append('<th colspan="' + str(bench_width()) + '">' + str(value) + '</th>')
    outarr.append('</tr>')

    return '\n'.join(outarr)

'''

        <tr align="center">
            <td>tanhdp</td>
            <td bgcolor="#66CC66">Passed</td>
            <td align="right">720 </td>
            <td align="right">218 </td>
            <td align="right">218 </td>
            <td align="right">159 </td>
            <td align="right">56  </td>
            <td align="right">1536/td&gt;
            </td><td align="right">1536</td>
            <td align="right">960 </td>
        </tr>
'''

def html_td(value, align=None, bgcolor=None):
    outarr = ['<td']

    if align:   outarr.append('align="' + str(align) + '"')
    if bgcolor: outarr.append('bgcolor="' + str(bgcolor) + '"')

    outarr.append('>')
    outarr.append(str(value))
    outarr.append('</td>')

    return ' '.join(outarr)


def format_kernel_entry(kernel_name, kernel_data):
    outarr = []

    outarr.append('<tr align="center">')

    outarr.append(html_td(kernel_name))

    color = green_rgb if kernel_data['Result'] == 'Passed' else red_rgb
    outarr.append(html_td(kernel_data['Result'], bgcolor=color))

    for field in profile_fields:
        outarr.append(html_td(kernel_data['Profile'][field], align='right'))

    for field in memory_fields:
        outarr.append(html_td(kernel_data['Memory'][field], align='right'))
    
    outarr.append('</tr>')

    return '\n'.join(outarr)

def format_notes():
    outarr = []
    outarr.append("""      <table frame="box" rules="none" bgcolor="#ffff99" border="1" cellpadding="4" cellspacing="2" width="50%"> 

        <tbody><tr> 
          <td rowspan="2" align="left">Notes:</td> 
          <td align="left">""")

    for line in NOTES:
        outarr.append('<li>' + line + '</li>')

    outarr.append("""		  </td>
        </tr>  

      </tbody></table>  
    </p>""")

    return '\n'.join(outarr)

def format_minor_header(name):
    return '<th bgcolor="#cccccc">&nbsp;&nbsp;' + name + '&nbsp;&nbsp;</th>'

def format_major_header(name, size):
    return '<th colspan="' + str(size) + '" bgcolor="#cccccc">' + name + '</th>'

def format_test_result(test):
    """Generates results for single test as HTML string"""
    
    table_header_fields = ['Precision', 'Endianness', 'Object File Format',
                           'Simulator', 'Cores Used', 'CCS Version', 'CG TOOLS Version']
    
    test_name_line = '<a name="' + test['test_name'] + '"></a>'
    
    outarr = []
    outarr.append('<div>')
    outarr.append('<h1>&nbsp;</h1>')
    outarr.append(test_name_line)
    outarr.append('<table bgcolor="#ffffff" border="1" cellpadding="4" cellspacing="2" width="75%">')

    outarr.append('<tr bgcolor="#999999" align="left">')
    outarr.append('<th colspan="' + str(full_width()) + '"><big>Test Parameters</big></th>')
    outarr.append('</tr>')
    
    for field in table_header_fields:
        outarr.append(format_table_header_entry(field, test[field]))

    ## Print major column headers
    outarr.append('<tr align="center">')
    outarr.append('<th rowspan="2" bgcolor="#cccccc">Kernel</th>')
    outarr.append('<th rowspan="2" bgcolor="#cccccc">Result</th>')
    outarr.append(format_major_header('Profile (Cycles)', len(profile_fields)))
    outarr.append(format_major_header('Memory (Bytes)', len(memory_fields)))
    outarr.append('</tr>')

    ## Print minor column headers
    outarr.append('<tr align="center">\n')
    for field in profile_fields:
        outarr.append(format_minor_header(field))
        
    for field in memory_fields:
        outarr.append(format_minor_header(field))
    outarr.append('</tr>\n')

    ## Print all kernel information
    for kernel_name, kernel_data in sorted(test['Kernels'].iteritems()):
        outarr.append(format_kernel_entry(kernel_name, kernel_data))
    
    outarr.append('</tbody></table>\n' +
                  '<p><a href="#HOME">Top</a>\n'
                  '</p></div>\n')

    return '\n'.join(outarr)

def get_title(proc):
    return ' '.join([library, version, proc])

def generate_report(data, proc):
    """Horribly hardcoded method of formatting final results dict"""
    outarr = []

    outarr.append("""<html><head> 
    <meta http-equiv="Content-Language" content="en-us"> 
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"> 
    <title>Project Runner Data</title> 
  </head>  

  <body>  
    <a name="HOME"></a> 

    <h1 align="center">Texas Instruments Test Results</h1>""")

    
    outarr.append('<h1 align="center">' + get_title(proc) + '</h1>')

    outarr.append("""<h1></h1>   
    
    <div> 
      <h1>&nbsp;</h1>
      <h1>Session Summary</h1>   
      <table bgcolor="#ffffff" border="1" cellpadding="4" cellspacing="2" width="50%"> 

        <tbody><tr align="center" bgcolor="#999999"> 
          <th>Precision</th> 
          <th>Endianness</th> 
          <th>Object Format</th> 
          <th>Link</th> 
        </tr>""")

    # there are millions of better ways to do this...but here we are
    if '674' in proc:
        outarr.append("""        
	<tr align="left"> 
          <td rowspan="2">Double Float</td> 
          <td rowspan="2">Little Endian</td> 
          <td>COFF Format</td> 
          <td><a href="#DLC">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td>ELF Format</td> 
          <td><a href="#DLE">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td rowspan="2">Single Float</td> 
          <td rowspan="2">Little Endian</td> 
          <td>COFF Format</td> 
          <td><a href="#SLC">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td>ELF Format</td> 
          <td><a href="#SLE">View Results</a></td> 
        </tr>
        """)
    else:
        outarr.append("""<tr align="left"> 
          <td rowspan="4">Double Float</td> 
          <td rowspan="2">Big Endian</td> 
          <td>COFF Format</td> 
          <td><a href="#DBC">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td>ELF Format</td> 
          <td><a href="#DBE">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td rowspan="2">Little Endian</td> 
          <td>COFF Format</td> 
          <td><a href="#DLC">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td>ELF Format</td> 
          <td><a href="#DLE">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td rowspan="4">Single Float</td> 
          <td rowspan="2">Big Endian</td> 
          <td>COFF Format</td> 
          <td><a href="#SBC">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td>ELF Format</td> 
          <td><a href="#SBE">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td rowspan="2">Little Endian</td> 
          <td>COFF Format</td> 
          <td><a href="#SLC">View Results</a></td> 
        </tr>  

        <tr align="left"> 
          <td>ELF Format</td> 
          <td><a href="#SLE">View Results</a></td> 
        </tr>""")
    

    outarr.append("""</tbody></table>  
      
      <p>
      </p><p>
    
    """)

    outarr.append(format_notes())

    outarr.append('</div>')

    outarr.append('<html><head>\n' +
                  '<meta http-equiv="Content-Language" content="en-us">\n' +
                  '<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">\n' +
                  '<title>Project Runner Data</title>\n' +
                  '</head>\n')

    
    
    for key,value in sorted(data.iteritems()):
        if 'test_name' in value:
            outarr.append(format_test_result(value))

    outarr.append('</body></html>')
            
    outstring = '\n'.join(outarr)
    
    return outstring
    
##------------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])

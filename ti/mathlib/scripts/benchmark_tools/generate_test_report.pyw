import sys
from lib import build_model
from lib import model_to_html

# realistically this should be better, but there is a good chance this never gets used again.
# sorry person of the future

## Acceptable Parameters
acceptable_parameters = {
    'c66':'Benchmark only c66x build.',
    'c674':'Benchmark only c674x build.',
    'all':'Benchmark all builds.'
}

def main(argv):
    if not check_argv(argv):
        # generates a JSON object representing the data
        build_model.main(argv)
        
        # generates HTML view of said object
        model_to_html.main(argv)


def check_argv(argv):
    """Checks main parameters, prints error message/returns True if error."""
    if not argv or argv[0] not in acceptable_parameters.keys():
        print "* ERROR: Parameters"
        if argv: print "* Invalid option: " + str(argv[0])
        print "* Acceptable usage: 'generate_test_report.pyw [" +\
            " ".join(sorted(acceptable_parameters.keys())) +\
            "]'"
        for key, value in sorted(acceptable_parameters.iteritems()):
            print "* - " + key + ": " + value
        return True

        
if __name__ == "__main__":
    main(sys.argv[1:])

    

#!/bin/bash

## Create array of commit ids from input
export commit_array=`echo $1 | sed -e 's/,//g'`

## Get user ID
export user_id=`env | grep USERNAME | sed -e 's/.*=//g'`

## Loop through each commit ID and cherry-pick
for commit_id in ${commit_array[@]}; do
export sub_commit_id=`echo $commit_id | tail -c 3`
git fetch ssh://$user_id@gerrit.ext.ti.com:29418/vision/vlib refs/changes/$sub_commit_id/$commit_id/1 && git cherry-pick FETCH_HEAD
done

## Nothing past this point
